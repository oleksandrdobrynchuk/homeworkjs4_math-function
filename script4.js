// 1.Описати своїми словами навіщо потрібні функції у програмуванні.
//Функці допомагають написати нам один раз код і використовувати його в любому місці документа.

// 2.Описати своїми словами, навіщо у функцію передавати аргумент.
//Аргументи необхідні для того, щоб передати їх у параметри, які виккликає функція.

// 3.Що таке оператор return та як він працює всередині функції?
//Данний оператор повертає результат обчислень функції. Тобто видає результат операцїї, яка прописана у функції.



//Завдання

let inputFirstNumber = document.querySelector('.input-number-one');
let inputSecondNumber =document.querySelector('.input-number-two');
let inputOperator = document.querySelector('.action-input');

function count(firstNumber, secondNumber, operator) {


    // Option #1
    // switch (operator) {
    //     case "*":
    //         return firstNumber * secondNumber;
    //     case "+":
    //         return firstNumber + secondNumber;
    //     case "-":
    //         return firstNumber - secondNumber;
    //     case "/":
    //         return firstNumber / secondNumber;
    // }


    // Option #2
    if (operator === '+') {
        return firstNumber + secondNumber;
    } else if (operator === '-') {
        return firstNumber - secondNumber;
    }else if (operator === '*') {
        return firstNumber * secondNumber;}
    else if (operator === '/') {
        return firstNumber / secondNumber;}
}


document.querySelector(".button").addEventListener("click", () => {
    document.querySelector(".output").innerText = "Result: " + count(+inputFirstNumber.value, +inputSecondNumber.value, inputOperator.value);
});



